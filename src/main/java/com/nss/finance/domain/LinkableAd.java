package com.nss.finance.domain;

import org.springframework.hateoas.Identifiable;

/**
 * Created by NSS on 02.06.2016.
 */
public interface LinkableAd extends Identifiable<Long> {
    Ad.Status getStatus();
}
