package com.nss.finance.domain;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by NSS on 02.06.2016.
 */
public class AdValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Ad.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final Ad ad = (Ad)target;
        if(ad.getAmount() == null || ad.getAmount().intValue() <= 0) {
            errors.rejectValue("amount", "Ad.amount.invalid", "Amount must be positive");
        }
    }
}
