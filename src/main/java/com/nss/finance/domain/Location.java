package com.nss.finance.domain;

import javax.persistence.*;

/**
 * Created by NSS on 01.06.2016.
 */

@Embeddable
public class Location {

    @Column(nullable = false)
    private  String city;

    private String area;

    public Location() {}
    public Location(String city, String area) {
        this.setCity(city);
        this.setArea(area);
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Location)) {
            return false;
        }

        Location location = (Location) object;

        if (getCity() != null ? !getCity().equals(location.getCity()) : location.getCity() != null) {
            return false;
        }
        return getArea() != null ? getArea().equals(location.getArea()) : location.getArea() == null;

    }

    @Override
    public int hashCode() {
        int result = getCity() != null ? getCity().hashCode() : 0;
        result = 31 * result + (getArea() != null ? getArea().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Location{" +
                "city='" + city + '\'' +
                ", area='" + area + '\'' +
                '}';
    }
}
