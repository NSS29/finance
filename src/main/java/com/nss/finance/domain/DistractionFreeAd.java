package com.nss.finance.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

/**
 * Created by NSS on 02.06.2016.
 */

@Projection(types = Ad.class, name = "minimal")
public interface DistractionFreeAd extends LinkableAd {

    Ad.Type getType();

    BigDecimal getAmount();

    String getCurrency();

    String getRate();

    @Value("#{target.user?.phoneNumber}")
    String getPhoneNumber();
}
