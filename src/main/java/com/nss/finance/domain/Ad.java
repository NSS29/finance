package com.nss.finance.domain;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.hateoas.Identifiable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * Created by NSS on 01.06.2016.
 */

@Entity
@Table(name = "ads")
public class Ad implements LinkableAd {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;

    public enum Type {
        BUY,
        SELL
    }

    @Column(nullable = false)
    private BigInteger amount;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    public enum Currency {
        USD,
        EUR
    }

    @Column(nullable = false)
    private BigDecimal rate;


    private LocalDateTime publishedAt;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    public enum Status {
        NEW,
        PUBLISHED,
        EXPIRED
    }

    public void publish() {
        if(getStatus() == Status.NEW) {
            publishedAt = LocalDateTime.now();
            setStatus(Status.PUBLISHED);
        } else {
            throw new InvalidAdStateTransitionException("Ad is already published");
        }
    }

    public void expire() {
        if(getStatus() == Status.PUBLISHED) {
            setStatus(Status.EXPIRED);
        } else {
            throw new InvalidAdStateTransitionException(
                    "Ad can be finished only when it is in the " + Status.PUBLISHED);
        }
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    public static class InvalidAdStateTransitionException extends RuntimeException {
        public InvalidAdStateTransitionException(String message) {
            super(message);
        }
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User user;

    @Embedded
    private Location location;

    @Version
    private Integer version;

    @LastModifiedDate
    private LocalDateTime lastModefied;

    public Ad() {}

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDateTime getLastModefied() {
        return lastModefied;
    }

    public void setLastModefied(LocalDateTime lastModefied) {
        this.lastModefied = lastModefied;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Ad)) {
            return false;
        }

        Ad ad = (Ad) object;

        if (getType() != ad.getType()) {
            return false;
        }
        if (getAmount() != null ? !getAmount().equals(ad.getAmount()) : ad.getAmount() != null) {
            return false;
        }
        if (getCurrency() != ad.getCurrency()) {
            return false;
        }
        if (getRate() != null ? !getRate().equals(ad.getRate()) : ad.getRate() != null) {
            return false;
        }
        return getUser() != null ? getUser().equals(ad.getUser()) : ad.getUser() == null;

    }

    @Override
    public int hashCode() {
        int result = getType() != null ? getType().hashCode() : 0;
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getCurrency() != null ? getCurrency().hashCode() : 0);
        result = 31 * result + (getRate() != null ? getRate().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "type=" + type +
                ", amount=" + amount +
                ", currency=" + currency +
                ", user=" + user +
                '}';
    }
}
