package com.nss.finance.integration;

import com.nss.finance.domain.Ad;
import com.nss.finance.domain.User;
import com.nss.finance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import org.springframework.security.access.SecurityConfig;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by NSS on 02.06.2016.
 */

@Component
@RepositoryEventHandler
public class AdEventHandler {


    @Autowired
    private UserRepository userRepository;

    @HandleBeforeCreate
    @HandleAfterSave
    public void setLastModifiedTime(Ad ad) {
        ad.setLastModefied(LocalDateTime.now());
    }

    @HandleBeforeCreate
    public void setOwner(Ad ad) {
        final String phoneNumber = SecurityContextHolder.getContext().getAuthentication().getName();
        ad.setUser(userRepository.findByPhoneNumber(phoneNumber));
    }
}
