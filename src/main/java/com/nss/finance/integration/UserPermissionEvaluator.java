package com.nss.finance.integration;

import com.nss.finance.domain.User;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by NSS on 09.06.2016.
 */

@Component
public class UserPermissionEvaluator implements PermissionEvaluator {

    private static final SimpleGrantedAuthority ADMIN_AUTHORITY = new SimpleGrantedAuthority("ADMIN");

    @Override
    public boolean hasPermission(Authentication authentication, Object target, Object permission) {

        if(target instanceof User) {
            User user = (User) target;
            String phoneNumber = user.getPhoneNumber();
            if("delete".equals(permission)) {
                return isAdmin(authentication)
                        || phoneNumber.equals(authentication.getName());
            }

            throw new UnsupportedOperationException(
                    "hasPermission not supported for object <" + target
                            + "> and permission <" + permission + ">");
        }
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        return false;
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(ADMIN_AUTHORITY);
    }
}
