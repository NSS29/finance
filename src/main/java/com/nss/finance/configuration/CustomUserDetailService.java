package com.nss.finance.configuration;

import com.nss.finance.domain.User;
import com.nss.finance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NSS on 08.06.2016.
 */

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        final User user = userRepository.findByPhoneNumber(userName);
        return new org.springframework.security.core.userdetails.User(
                user.getPhoneNumber(), user.getPassword(),
                true, true, true, true, getAuthorities(user.getRoles()));
    }

    public List<GrantedAuthority> getAuthorities(List<String> roles) {
        List<GrantedAuthority> authList = new ArrayList();
        for(String role : roles) {
            authList.add(new SimpleGrantedAuthority(role));
        }
        return authList;
    }
}
