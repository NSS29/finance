package com.nss.finance.configuration;

import com.nss.finance.domain.AdValidator;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

/**
 * Created by NSS on 02.06.2016.
 */

@Configuration
public class CustomRepositoryRestConfigurer extends RepositoryRestConfigurerAdapter {

    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", new AdValidator());
    }
}
