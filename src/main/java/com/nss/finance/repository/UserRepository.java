package com.nss.finance.repository;

import com.nss.finance.domain.User;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;

import java.util.List;

/**
 * Created by NSS on 01.06.2016.
 */

public interface UserRepository extends CrudRepository<User, Long> {


    User findOne(Long id);


    Iterable<User> findAll(Iterable<Long> longs);

    User findByPhoneNumber(@Param("number") String phoneNumber);

    @PreAuthorize("isAuthenticated()")
    @PostFilter("filterObject.phoneNumber == principal.username")
    Iterable<User> findAll();

    @CachePut(value="financeCache", key="#result.id")
    User save(User user);

    //@RestResource(exported = false)
    void delete(Long aLong);

//    @PreAuthorize("hasAnyRole({'USER', 'ADMIN'})")
//    @PreFilter( "hasRole('ADMIN') || "
//            + "targetObject.phoneNumber == principal.username")
    @PreAuthorize("hasAnyRole({'ROLE_SPITTER', 'ROLE_ADMIN'})")
    @PreFilter("hasPermission(targetObject, 'delete')")
    void delete(Iterable<? extends User> users);
}
