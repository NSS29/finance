package com.nss.finance.repository;

import com.nss.finance.domain.Ad;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by NSS on 01.06.2016.
 */
@RepositoryRestResource(path = "ads")
public interface AdRepository extends PagingAndSortingRepository<Ad, Long> {

    @Query("select ad from Ad ad where ad.status = 'PUBLISHED'")
    @RestResource(path = "published")
    Page<Ad> findPublised(Pageable pageable);

    @Query("select ad from Ad ad where ad.user.phoneNumber = ?#{principal?.username}")
    @RestResource(path = "my")
    List<Ad> findMyAds();
}
